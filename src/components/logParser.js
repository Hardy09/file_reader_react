﻿import axios from 'axios';
import React, { useState } from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import { Spinner, Button, Container } from "react-bootstrap";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const LogParser = () => {
    const [loading, setLoading] = useState(false);
    const [file, setFile] = useState("");

    const handleFileChange = async (e) => {
        const { files } = e.target;
        setFile(files[0]);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (!file) {
            return toast.error("Please choose file!", { toastId: "FILE" })
        }
        setLoading(true)

        var formdata = new FormData();
        const inputValues = {
            image: file
        }

        Object.keys(inputValues).forEach(key => {
            formdata.append(key, inputValues[key])
        })

        let url = process.env.REACT_APP_BASE_URL + `api/log-parser`

        axios.post(url, formdata)
            .then(response => {
                if(response?.data?.result?.msg == 'error'){
                    toast.error("Please upload Readable file only!", { toastId: "API" })
                    setLoading(false)
                    return
                }
                const jsonString = `data:text/json;chatset=utf-8,${encodeURIComponent(
                    JSON.stringify(response?.data?.result?.data)
                )}`;
                const link = document.createElement("a");
                link.href = jsonString;
                link.download = "data.txt";

                link.click();
                toast.success("Successfully Downloaded!", { toastId: "API" })
                setLoading(false)
            })
            .catch((err) => {
                toast.error("Please upload Readable file only!", { toastId: "API" })
                setLoading(false)
                console.log("err", err)
            });


    }

    return (
        <>
            <ToastContainer />
            <Container className="my-5">
                <form onSubmit={handleSubmit}>
                    <h3 className='text-center'>Log parser</h3>
                    <div className='m-3'>
                        <label>Choose file:</label>
                        <input className='mx-3' type="file" onChange={handleFileChange}></input>
                    </div>
                    <div className='m-3'>
                        {loading ?
                            <Button disabled style={{ "width": "120px" }}>
                                <span style={{ "float": "left" }}>Upload</span>
                                <Spinner
                                    style={{
                                        "position": "absolute",
                                        "width": "25px",
                                        "height": "25px"
                                    }}
                                    animation="border"
                                    variant="danger"
                                />
                            </Button>
                            : <Button type="submit">Upload </Button>
                        }
                    </div>
                </form>
            </Container>
        </>
    )
}

export default LogParser