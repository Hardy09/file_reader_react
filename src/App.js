import { BrowserRouter, Routes, Route } from "react-router-dom";
import LogParser from "./components/logParser";

export default function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route exact path="/" element={<LogParser />} />
        </Routes>
      </BrowserRouter>

    </>
  );
}